﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameMaster : MonoBehaviour {

    [System.Serializable]
    public class Infection
    {
        public float totalSeconds;
        public Slider infectionTimer;
        public Text text;
    }



    [System.Serializable]
    public class Lives
    {
        public Image livesImage;
        public Sprite lives0;
        public Sprite lives1;
        public Sprite lives2;
        public Sprite lives3;
        public Sprite lives4;
    }

    public Infection infection;
    public Lives lives;

    private int infectorCount;
    private float secondsPercentage;
    private float secondsToGo;
    private GameObject[] infectors;
    
    #region Unity Methods
    //called at the beginning of the game
    void Awake()
    {
        GameController.Target = GameObject.FindGameObjectWithTag(TagController.player);
        GameController.Moveable = false;
        secondsToGo = infection.totalSeconds;
    }

    //called every 0.02 seconds (interval can be changed)
    void FixedUpdate()
    {
        #region Player Health
        if (PlayerController.Health > 4)
        {
            PlayerController.Health = 4;
        }
        if (PlayerController.Death)
        {
            PlayerController.Health = 0;
        }
        switch (PlayerController.Health)
        {
            case 0:
                lives.livesImage.sprite = lives.lives0;
                break;
            case 1:
                lives.livesImage.sprite = lives.lives1;
                break;
            case 2:
                lives.livesImage.sprite = lives.lives2;
                break;
            case 3:
                lives.livesImage.sprite = lives.lives3;
                break;
            case 4:
                lives.livesImage.sprite = lives.lives4;
                break;
            default:
                lives.livesImage.sprite = lives.lives0;
                break;
        }
        #endregion
    }

    //called every frame
    void Update()
    {
        #region Infection Timer
        int temp;
        if (GameController.Moveable)
        {
            infectors = GameObject.FindGameObjectsWithTag(TagController.infector);
            infectorCount = infectors.Length;
            foreach (GameObject infector in infectors)
            {
                if (!Util.IsRendererActive(infector))
                {
                    infectorCount--;
                }
            }
            secondsToGo -= 1.0f * InfectionSpeed() * Time.deltaTime;
        }
        secondsPercentage = 100 * (1 / (infection.totalSeconds / secondsToGo));
        GameController.TimerSeconds = secondsToGo;
        infection.infectionTimer.value = 100 - secondsPercentage;
        temp = Mathf.RoundToInt(infection.infectionTimer.value);
        infection.text.text = temp + "%";

        NoTimeLeft();
        #endregion
    }
    #endregion

    //restarts the level if time is over
    private void NoTimeLeft()
    {
        if (secondsToGo <= 0)
        {
            GameController.Won = false;
            GameController.DataReset();
            Application.LoadLevel(SceneController.GetLevel);
        }
    }

    //sets the infection speed
    private float InfectionSpeed()
    {
        if (infectorCount <= 0)
        {
            return 0.0f;
        }
        else if (infectorCount <= 3)
        {
            return 1.0f;
        }
        else if (infectorCount <= 6)
        {
            return 1.3f;
        }
        else if (infectorCount <= 9)
        {
            return 1.6f;
        }
        else if (infectorCount <= 12)
        {
            return 2.0f;
        }
        else
        {
            return 2.5f;
        }
    }

}
