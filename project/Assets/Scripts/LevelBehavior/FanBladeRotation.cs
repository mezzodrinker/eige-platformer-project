﻿using UnityEngine;
using System.Collections;

public class FanBladeRotation : MonoBehaviour {

    public float bladeSpeed;
    public GameObject dust;

    #region Unity Methods
    // Update is called once per frame
    void Update()
    {
        //rotates the fan with a set speed
        transform.Rotate(0, 0, bladeSpeed * Time.deltaTime);

        //if the fan is rotating you can see dust
        if (dust != null)
        {
            dust.SetActive(bladeSpeed > 0);
        }
    }
    #endregion

}
