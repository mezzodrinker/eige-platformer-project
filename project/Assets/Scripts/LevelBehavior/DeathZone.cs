﻿using UnityEngine;
using System.Collections;

public class DeathZone : MonoBehaviour
{

    #region Unity Methods
    //called whenever an other collider hits the objects trigger collider
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == GameController.Target)
        {
            //reset data
            GameController.DataReset();
            //reload the actual level
            Application.LoadLevel(SceneController.GetLevel);
        }
    }
    #endregion

}
