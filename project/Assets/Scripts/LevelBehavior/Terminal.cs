﻿using UnityEngine;
using System.Collections;

public class Terminal : MonoBehaviour
{
    public GameObject fan;
    private FanElevator elevatePlayer;
    private FanBladeRotation rotateFan;
    private float strength = 0.0f;

    public void setStrength(float strength)
    {
        this.strength = strength;
    }

    #region Unity Methods
    //called at the beginning of the game
    void Awake()
    {
        elevatePlayer = fan.GetComponent<FanElevator>();
        rotateFan = fan.GetComponentInChildren<FanBladeRotation>();
    }

    //called every frame
    void Update()
    {
        elevatePlayer.strength = strength/10;
        rotateFan.bladeSpeed = strength * 50;
        Debug.Log(GameController.PlayerInteractable);
    }

    //called whenever an other collider hits the objects trigger collider
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == GameController.Target)
        {
            TerminalController.NearTerminal = true;
        }
    }

    //called whenever an other collider leaves the objects trigger collider
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == GameController.Target)
        {
            TerminalController.NearTerminal = false;
        }
    }
    #endregion

}
