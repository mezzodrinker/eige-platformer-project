﻿using UnityEngine;
using System.Collections;

public class RingRotation : MonoBehaviour {
    public int axis;
    public float speed;

    #region Unity Methods
    // Update is called once per frame
	void Update () {
        if (axis == 0)
        {
            transform.Rotate(new Vector3(speed * Time.deltaTime, 0f, 0f));
        }
        if (axis == 1)
        {
            transform.Rotate(new Vector3(0f, speed * Time.deltaTime, 0f));
        }
        if (axis == 2)
        {
            transform.Rotate(new Vector3(0f, 0f, speed * Time.deltaTime));
        }
    }
    #endregion

}
