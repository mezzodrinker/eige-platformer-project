﻿using UnityEngine;
using System.Collections;

public class PlayerCollision : MonoBehaviour
{

    #region Unity Methods
    //called whenever an other collider hits the objects trigger collider
    void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case TagController.defender:
                //TODO: interact with the defender
                PlayerController.Health--;
                break;
            case TagController.defenderTrojan:
                //TODO: interact with the trojan defender
                PlayerController.Health--;
                break;
            case TagController.infector:
                //TODO: interact with the infector
                PlayerController.Health--;
                break;
            case TagController.infectorTrojan:
                //TODO: interact with the trojan infector
                PlayerController.Health--;
                break;
            case TagController.trojan:
                //TODO: interact with the trojan
                PlayerController.Health--;
                break;
            case TagController.enemyProjectile:
                //TODO: interact with the enemy projectile
                PlayerController.Health--;
                break;
            default:
                break;
        }
    }
    #endregion

}
