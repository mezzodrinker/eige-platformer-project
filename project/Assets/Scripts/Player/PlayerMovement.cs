﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour {

    public int maxJumps;
    public float jumpHeight;
    public float playerSpeed;
    private int isWalkingHash;
    private int jumpCounter;
    private Animator playerAnim;
    private Rigidbody playerRigidbody;
    private Vector3 movement;

    public int JumpCounter
    {
        get
        {
            return jumpCounter;
        }
        set
        {
            jumpCounter = value;
        }
    }

    private void move(float h, float v)
    {
        //Rotate the player to the movement direction
        if (!(h == 0.0f && v == 0.0f))
        {
            transform.rotation = Quaternion.Euler(new Vector3(0.0f, RotatePlayer(h, v), 0.0f));
        }

        //Set the direction of the movement
        movement.Set(h, 0.0f, v);
        
        //Set the speed of the movement
        //normalizing is to prevent a speed of 1.4 in an 45 degree angular if the player presses both vertivcal and horizontal buttons
        movement = movement.normalized * playerSpeed * Time.deltaTime;
        
        //Move the player to the correct position
        playerRigidbody.MovePosition(transform.position + movement);
    }

    #region Unity Methods
    //called at the beginning of the game
    void Awake()
    {
        isWalkingHash = Animator.StringToHash("IsWalking");
        playerAnim = gameObject.GetComponent<Animator>();
        playerRigidbody = gameObject.GetComponent<Rigidbody>();
    }

    //called at the first frame the sccript is active
    void Start()
    {
        transform.position = PlayerController.PlayerPosition[SceneController.Level];
        transform.rotation = Quaternion.Euler(PlayerController.ZoomedRotation);
    }

    //called every frame
    void Update()
    {
        if (GameController.Moveable && !GameController.Shooting)
        {
            #region Moving
            //GetAxisRaw returns 1, -1 or 0
            float h = Input.GetAxisRaw("Horizontal");
            float v = Input.GetAxisRaw("Vertical");
            if (h != 0 || v != 0)
            {
                //walk animation if the player is walking
                playerAnim.SetBool(isWalkingHash, true);
            }
            else
            {
                //stop walking animation if the player is not walking
                playerAnim.SetBool(isWalkingHash, false);
            }
            move(h, v);
            #endregion

            #region Jumping
            //when the jump button is pressed and he didn't do too much jumps
            if (Input.GetButtonDown("Jump") && jumpCounter < maxJumps)
            {
                //the player has jumped
                JumpCounter++;
                //pushes the player with physics up
                playerRigidbody.AddForce(Vector3.up * jumpHeight);
            }
            #endregion
        }
        else
        {
            //stop walking animation if the player cannot move
            playerAnim.SetBool(isWalkingHash, false);
        }
        
    }
    #endregion

    private float RotatePlayer(float h, float v)
    {

        if (h != 0 && v == 0)
        {
            return h * 90;
        }
        else if (v != 0  && h==0)
        {
            return (v-1)*(-90);
        }
        else if (h != 0 && v != 0)
        {
            return 45*(v-2) * -h;
        }
        else return transform.rotation.y;
    }
}
