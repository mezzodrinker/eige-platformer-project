﻿using UnityEngine;
using System.Collections;

public class JumpCollider : MonoBehaviour
{

    private PlayerMovement playerMovement;

    #region Unity Methods
    //called at the beginning of the game
    void Awake()
    {
        playerMovement = GetComponentInParent<PlayerMovement>();
    }

    //called every frame
    void Update()
    {
        //to be sure the jump collider is always exactly under the players feet
        transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    }

    //called whenever an other collider hits the objects trigger collider
    void OnTriggerEnter(Collider other)
    {
        //if the player is standing on something he should be able to jump again
        playerMovement.JumpCounter = 0;
    }

    //called whenever an other collider leaves the objects trigger collider
    void OnTriggerExit(Collider other)
    {
        //player just should be able to interact if he is standing on specific platforms
        GameController.PlayerInteractable = false;
    }

    //called as long as an other collider stays in the objects trigger collider
    void OnTriggerStay(Collider other)
    {
        //player just should be able to interact if he is standing on specific platforms
        GameController.PlayerInteractable = checkTag(other);
    }
    #endregion

    //check tag of the colliders gameObject-tag
    private bool checkTag(Collider other)
    {
        Debug.Log(other.gameObject.tag);
        switch (other.gameObject.tag)
        {
            case TagController.platform:
                return true;
            case TagController.platformInvisible:
                return true;
            default:
                return false;
        }
    }

}
