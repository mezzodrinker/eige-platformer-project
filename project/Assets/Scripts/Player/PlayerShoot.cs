﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour
{

    public Animator playerAnim;
    public GameObject bullet;
    public bool fire;
    public int bulletSpeed;

    private bool buf;
    private int isShootingHash;
    private Vector3 bulletPos;

    #region Unity Methods
    //called at the beginning of the game
    void Awake()
    {
        buf = false;
        fire = false;
        isShootingHash = Animator.StringToHash("IsShooting");
        bulletPos = bullet.transform.position;
    }

    //called at the first frame the script is active
    void Start()
    {
        bullet.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    }

    //called every frame
    void Update()
    {
        //if the player is pressing the shoot button, is moveable and is not already shooting
        if (Input.GetButtonDown("Shoot") && GameController.Moveable && !GameController.Shooting)
        {
            //starts the shooting animation
            GameController.Shooting = true;
            playerAnim.SetBool(isShootingHash, true);
        }

        //fire is set true/false in the animation
        if (fire)
        {
            //enables the projectile
            bullet.SetActive(true);
            //moves the projectile
            bullet.transform.localPosition = bullet.transform.localPosition + (new Vector3(0.0f, -1.0f, 0.0f) * bulletSpeed * Time.deltaTime);
            buf = true;
        }
        else
        {
            //disables the projectile
            bullet.SetActive(false);
            //sets the projectile to its start position
            bullet.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
            //only directly after a shoot
            if (buf)
            {
                //stops the shooting animation
                playerAnim.SetBool(isShootingHash, false);
                buf = false;
                //player isn't shooting anymore
                GameController.Shooting = false;
            }
        }
    }
    #endregion

}
