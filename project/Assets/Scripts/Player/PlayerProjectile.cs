﻿using UnityEngine;
using System.Collections;

public class PlayerProjectile : MonoBehaviour
{

    private PlayerShoot playerShoot;

    #region Unity Methods
    //called at the first frame the script is active
    void Start()
    {
        playerShoot = gameObject.GetComponentInParent<PlayerShoot>();
    }

    //called whenever an other collider hits the objects trigger collider
    void OnTriggerEnter(Collider other)
    {
        //destroy the other object if it is an enemy
        switch (other.tag)
        {
            case TagController.defender:
                DestroyOther(other);
                break;
            case TagController.defenderTrojan:
                DestroyOther(other);
                break;
            case TagController.infector:
                DestroyOther(other);
                break;
            case TagController.infectorTrojan:
                DestroyOther(other);
                break;
            case TagController.trojan:
                DestroyOther(other);
                break;
            default:
                break;
        }
    }
    #endregion

    //destroy other object
    void DestroyOther(Collider other)
    {
        Destroy(other.gameObject);
        playerShoot.fire = false;
    }

}
