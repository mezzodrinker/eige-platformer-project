﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class MainCamera : MonoBehaviour {

    private bool interaction;
    private bool prePos;
    private bool rotated;
    private bool zoomed;
    public GameObject terminalMenu;
    public GameObject menu;
    public GameObject HUD;
    private bool terminalMenuActive = false;
    private bool menuActive = false;

    //return from terminalMenu to normal scene
    public void setActive(bool active)
    {
        terminalMenuActive = active;
        prePos = false;
        zoomed = false;
    }

    //return from pauseMenu to normal scene
    public void setMenuActive(bool active)
    {
        menuActive = active;
        prePos = false;
        zoomed = false;
    }

    #region Unity Methods
    //called at the beginning of the game
    void Awake()
    {
        terminalMenu.SetActive(false);
        menu.SetActive(false);
        prePos = false;
        zoomed = false;
    }

    //called at the first frame the script is active
    void Start()
    {
        transform.position = GameController.Target.transform.position + CameraController.RelCamPosZoomedPost;
        transform.rotation = Quaternion.Euler(CameraController.CamRotZoomed);
    }

    //called every frame
    void Update()
    {
        #region HUD selection
        //chooses the right HUD
        terminalMenu.SetActive(terminalMenuActive);
        menu.SetActive(menuActive);
        if (terminalMenuActive || menuActive)
        {
            HUD.SetActive(false);
        }
        else
        {
            HUD.SetActive(true);
        }
        #endregion

        #region PlayerInteraction
        //player interaction or pause menu are just enabled if the player is movable and is standing on a normal ór invincible platform
        if (((Input.GetButtonDown("Player Interaction") && TerminalController.NearTerminal) || Input.GetKeyDown(KeyCode.Escape)) && GameController.PlayerInteractable && GameController.Moveable)
        {
            //is the player interacting or pausing?
            if (Input.GetButtonDown("Player Interaction"))
            {
                interaction = true;
            }
            else
            {
                interaction = false;
            }
            GameController.Moveable = false;
            zoomed = !zoomed;
            prePos = !prePos;
        }
        //checks if the player rotation is ready for interacting
        rotated = GameController.Target.transform.rotation == Quaternion.Euler(PlayerController.ZoomedRotation);
        #endregion
    }

    //called after the last update per frame
    void LateUpdate()
    {
        #region GameStart/MenuReturn
        //when the game is starting or the player returns from a menu
        if (!zoomed)
        {
            //zooming directly out of the screen to the position from where the camera is zooming to the normal position
            if (!prePos)
            {
                TransformPosAndRot(CameraController.RelCamPosZoomedPre, CameraController.CamRotZoomed, 3.0f, 0.005f);
                prePos = TestPosition(CameraController.RelCamPosZoomedPre);
            }
            //zooming to the normal camera position
            else
            {
                TransformPosAndRot(CameraController.RelCamPosNormal, CameraController.CamRotNormal, 3.0f, 0.05f);
                if (Vector3.Distance(transform.position, GameController.Target.transform.position + CameraController.RelCamPosNormal) <= 3.0f)
                {
                    GameController.Moveable = true;
                }
            }
        }
        #endregion
        #region Interaction/Pause
        //when the player is interacting or pausing
        else
        {
            //when the player is looking towards the camera
            if (rotated)
            {
                //zooming to the position from where the camera is directly zooming into the screen
                if (!prePos)
                {
                    TransformPosAndRot(CameraController.RelCamPosZoomedPre, CameraController.CamRotZoomed, 3.0f, 0.05f);
                    prePos = TestPosition(CameraController.RelCamPosZoomedPre);
                }
                //zooming directly into the screen
                else
                {
                    TransformPosAndRot(CameraController.RelCamPosZoomedPost, CameraController.CamRotZoomed, 10.0f, 0.005f);
                    if (TestPosition(CameraController.RelCamPosZoomedPost))
                    {
                        //opens the interaction menu
                        if (interaction)
                        {
                            terminalMenuActive = true;
                        }
                        //opens the pause menu
                        else
                        {
                            menuActive = true;
                        }
                    }
                }
            }
            //when the player looks anywhere but not towards the camera
            else
            {
                //rotates the player so he is looking towards the camera
                GameController.Target.transform.rotation = Quaternion.Euler(Vector3.Lerp(GameController.Target.transform.rotation.eulerAngles, PlayerController.ZoomedRotation, 5.0f * Time.deltaTime));
                if (Vector3.Distance(GameController.Target.transform.rotation.eulerAngles, PlayerController.ZoomedRotation) <= 3.0f)
                {
                    GameController.Target.transform.rotation = Quaternion.Euler(PlayerController.ZoomedRotation);
                }
            }
        }
        #endregion
    }
    #endregion

    //returns if the actual position is the target position (just the rel positions from the CameraController.cs)
    private bool TestPosition(Vector3 toTest)
    {
        return (transform.position == GameController.Target.transform.position + toTest);
    }

    //moves the camera from the actual position/rotation to a target position/rotation with a specific speed and a specific tolerance
    private void TransformPosAndRot(Vector3 targetPos, Vector3 targetRot, float smoothSpeed, float smoothTolerance)
    {
        transform.position = Vector3.Lerp(transform.position, GameController.Target.transform.position + targetPos, smoothSpeed * Time.deltaTime);
        transform.rotation = Quaternion.Euler(Vector3.Lerp(transform.rotation.eulerAngles, targetRot, smoothSpeed * Time.deltaTime));
        if (Vector3.Distance(transform.position, GameController.Target.transform.position + targetPos) <= smoothTolerance)
        {
            transform.position = GameController.Target.transform.position + targetPos;
            transform.rotation = Quaternion.Euler(targetRot);
        }
    }

}
