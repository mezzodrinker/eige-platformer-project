﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextOnButton : MonoBehaviour {
    private Text text;
    public float strength;

    public float Strength
    {
        set
        {
            strength = value;
        }
    }

	// Use this for initialization
	void Awake () {
        text = gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        text.text = strength*500 + " U/min";
	}
}
