﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WinMessage : MonoBehaviour
{

    private float counter;
    private string lose = "You Lose";
    private string win = "You Win";
    private Text winLoose;

    #region Unity Methods
    //called at the beginning of the game
    void Awake()
    {
        winLoose = gameObject.GetComponent<Text>();
    }

    //called at the first frame the script is active
    void Start()
    {
        winLoose.fontSize = 1;
        //choosing the correct message
        if (GameController.Won)
        {
            winLoose.text = win;
        }
        else
        {
            winLoose.text = lose;
        }
    }

    //called every 0.02 seconds (interval can be changed)
    void FixedUpdate()
    {
        //increases the font size until a specific value
        if (winLoose.fontSize < 102)
        {
            winLoose.fontSize++;
        }
    }
    #endregion

}
