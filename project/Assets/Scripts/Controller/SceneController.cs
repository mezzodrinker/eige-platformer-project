﻿using UnityEngine;
using System.Collections;

public class SceneController : MonoBehaviour {

    private static int level = 0;

    public static string GetLevel
    {
        get
        {
            return "Level" + level;
        }
    }

    public static int Level
    {
        get
        {
            return level;
        }
        set
        {
            level = value;
        }
    }

}
