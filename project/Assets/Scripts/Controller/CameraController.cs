﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    private static Vector3 camRotNormal = new Vector3(25.0f, 0.0f, 0.0f);
    private static Vector3 camRotZoomed = new Vector3(0.0f, 0.0f, 0.0f);
    private static Vector3 relCamPosNormal = new Vector3(0.0f, 7.0f, -10.0f);
    private static Vector3 relCamPosZoomedPost = new Vector3(0.0f, 1.07f, -0.7f);
    private static Vector3 relCamPosZoomedPre = new Vector3(0.0f, 1.0f, -1.2f);

    public static Vector3 CamRotNormal
    {
        get
        {
            return camRotNormal;
        }
    }

    public static Vector3 CamRotZoomed
    {
        get
        {
            return camRotZoomed;
        }
    }

    public static Vector3 RelCamPosNormal
    {
        get
        {
            return relCamPosNormal;
        }
    }

    public static Vector3 RelCamPosZoomedPost
    {
        get
        {
            return relCamPosZoomedPost;
        }
    }

    public static Vector3 RelCamPosZoomedPre
    {
        get
        {
            return relCamPosZoomedPre;
        }
    }

}
