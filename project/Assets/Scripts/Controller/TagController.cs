﻿using UnityEngine;
using System.Collections;

public class TagController {

    public const string controller        = "GameController";
    public const string player            = "Player";
    public const string infector          = "Infector";
    public const string defender          = "Defender";
    public const string trojan            = "Standard Trojan";
    public const string infectorTrojan    = "Infector Trojan";
    public const string defenderTrojan    = "Defender Trojan";
    public const string camera            = "MainCamera";
    public const string platform          = "Platform";
    public const string platformMoving    = "Platform (moving)";
    public const string platformRotating  = "Platform (rotating)";
    public const string platformInvisible = "Platform (invisible)";
    public const string platformDarksouls = "Platform (darksouls)";
    public const string trigger           = "Trigger";
    public const string button            = "Button";
    public const string playerProjectile  = "Player Projectile";
    public const string enemyProjectile   = "Enemy Projectile";
    public const string background        = "Background";

}
