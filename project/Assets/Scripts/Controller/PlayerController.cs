﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    private static int health = 4;
    private static Vector3 zoomedRotation = new Vector3(0.0f, 180.0f, 0.0f);
    private static Vector3[] playerPosition = { (new Vector3(0.0f, 0.0f, 0.0f)), (new Vector3(6.0f, 0.73f, -30.0f)) };

    public static bool Death
    {
        get
        {
            return (Health <= 0);
        }
    }

    public static int Health
    {
        get
        {
            return health;
        }
        set
        {
            health = value;
        }
    }

    public static Vector3 ZoomedRotation
    {
        get
        {
            return zoomedRotation;
        }
    }

    public static Vector3[] PlayerPosition
    {
        get
        {
            return playerPosition;
        }
        set
        {
            playerPosition = value;
        }
    }

}
