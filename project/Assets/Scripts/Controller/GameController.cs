﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

    private static bool hasKey = false;
    private static bool moveable = false;
    private static bool playerInteractable = true;
    private static bool shooting = false;
    private static bool won = false;
    private static float timerSeconds;
    private static GameObject target;

    //reset the necessary data to our default values
    public static void DataReset()
    {
        HasKey = false;
        Moveable = false;
        PlayerInteractable = true;
        Shooting = false;
        Won = false;
        PlayerController.Health = 4;
    }

    public static bool HasKey
    {
        get
        {
            return hasKey;
        }
        set
        {
            hasKey = value;
        }
    }

    public static bool Moveable
    {
        get
        {
            return moveable;
        }
        set
        {
            moveable = value;
        }
    }

    public static bool PlayerInteractable
    {
        get
        {
            return playerInteractable;
        }
        set
        {
            playerInteractable = value;
        }
    }

    public static bool ReverseTime {
        get
        {
            return Input.GetButton("ReverseTime");
        }
    }

    public static bool Shooting
    {
        get
        {
            return shooting;
        }
        set
        {
            shooting = value;
        }
    }

    public static bool Won
    {
        get
        {
            return won;
        }
        set
        {
            won = value;
        }
    }

    public static float TimerSeconds
    {
        get
        {
            return timerSeconds;
        }
        set
        {
            timerSeconds = value;
        }
    }

    public static GameObject Target
    {
        get
        {
            return target;
        }
        set
        {
            if (value == null)
            {
                Debug.LogError("target = null");
            }
            target = value;
        }
    }

}
