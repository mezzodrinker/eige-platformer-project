﻿using UnityEngine;
using System.Collections;

public class TerminalController : MonoBehaviour {

    private static bool nearTerminal;
    private static GameObject[] fans;

    public static bool NearTerminal
    {
        get
        {
            return nearTerminal;
        }
        set
        {
            nearTerminal = value;
        }
    }

    public static GameObject[] Fans
    {
        get
        {
            return fans;
        }
        set
        {
            fans = value;
        }
    }

}
