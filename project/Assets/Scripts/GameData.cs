﻿using UnityEngine;
using System.Collections;

public class GameData : MonoBehaviour {

    private static int score;
    private static int deathCount;
    public static bool paused;
    public static bool dead;

    public int Score
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
        }
    }

    public int DeathCount
    {
        get
        {
            return deathCount;
        }
        set
        {
            deathCount++;
        }
    }

}
