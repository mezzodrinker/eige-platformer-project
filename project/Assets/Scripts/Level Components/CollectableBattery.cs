﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class CollectableBattery : MonoBehaviour {

    #region Unity methods
    //called whenever an other collider hits the objects trigger collider
    public void OnTriggerEnter(Collider collider) {
        if (collider.gameObject.tag == TagController.player) {
            //increase the health by one
            PlayerController.Health += 1;
            Destroy(gameObject);
        }
    }

    //called whenever a collision with the object is detected
    public void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == TagController.player) {
            //increase the health by one
            PlayerController.Health += 1;
            Destroy(gameObject);
        }
    }
    #endregion

}
