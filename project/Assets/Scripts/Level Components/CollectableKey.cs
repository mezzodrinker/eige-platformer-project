﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class CollectableKey : MonoBehaviour {

    #region Unity methods
    //called whenever an other collider hits the objects trigger collider
    public void OnTriggerEnter(Collider collider) {
        if (collider.gameObject.tag == TagController.player) {
            //the player now has the key
            GameController.HasKey = true;
            Destroy(gameObject);
        }   
    }

    //called whenever a collision with the object is detected
    public void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == TagController.player) {
            //the player now has the key
            GameController.HasKey = true;
            Destroy(gameObject);
        }
    }
    #endregion

}
