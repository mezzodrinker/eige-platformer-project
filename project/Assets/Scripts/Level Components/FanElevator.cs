﻿using UnityEngine;
using System.Collections.Generic;

public class FanElevator : MonoBehaviour {

    private List<GameObject> toMove = new List<GameObject>();

    public bool isEnabled;
    public float range = 10.0f;
    public float strength = 1.0f;

    private Vector3 DirectionFacing {
        get {
            return transform.up;
        }
    }


    #region Unity methods
    //called every frame
    public void Update() {
        // if the game controller decides that stuff can't be moved around, exit this method
        if (!GameController.Moveable) {
            return;
        }

        // if this object's collider is not visible, exit this method (should prevent lag)
        if(!Util.IsVisible(gameObject, gameObject.collider.bounds)) {
            return;
        }

        Vector3 movement = DirectionFacing.normalized * strength;
        foreach (GameObject o in toMove) {
            if (o.rigidbody != null) {
                o.rigidbody.velocity += movement;
            } else {
                o.transform.position += movement;
            }
        }
    }

    //called whenever an other collider hits the objects trigger collider
    public void OnTriggerEnter(Collider collider) {
        if (!toMove.Contains(collider.gameObject)) {
            toMove.Add(collider.gameObject);
        }
    }

    //called whenever an other collider leaves the objects trigger collider
    public void OnTriggerExit(Collider collider) {
        toMove.Remove(collider.gameObject);
    }
    #endregion
}
