﻿using UnityEngine;
using System.Collections;

public class Win : MonoBehaviour
{

    #region Unity Methods
    //called whenever an other collider hits the objects trigger collider
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == GameController.Target)
        {
            GameController.Won = true;
            SceneController.Level++;
            Application.LoadLevel(SceneController.GetLevel);
        }
    }
    #endregion

}
