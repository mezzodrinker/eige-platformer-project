﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class VirusDefender : Virus {

    private int movingHash = Animator.StringToHash("Moving");
    private Animator defenderAnimator;
    private EnemyMovement enemyMovement;

    #region Unity Methods
    void Awake()
    {
        defenderAnimator = gameObject.GetComponent<Animator>();
        enemyMovement = gameObject.GetComponentInParent<EnemyMovement>();
    }

    override public void Update()
    {
        if (enemyMovement.Patrolling && GameController.Moveable)
        {
            defenderAnimator.SetBool(movingHash, true);
        }
        if (enemyMovement.Waiting)
        {
            defenderAnimator.SetBool(movingHash, false);
        }
    }
    #endregion
}
