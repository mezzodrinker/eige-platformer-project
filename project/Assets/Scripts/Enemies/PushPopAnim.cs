﻿using UnityEngine;
using System.Collections;

public class PushPopAnim : MonoBehaviour {
    int isCloseHash = Animator.StringToHash("IsClose");
    Animator anim;
	// Use this for initialization
	void Awake () {
        anim = gameObject.GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == TagController.player)
        {
            anim.SetBool(isCloseHash, true);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == TagController.player)
        {
            anim.SetBool(isCloseHash, false);
        }
    }
}
