﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Trojan : Enemy {
    protected internal int health;
    protected internal float timeSinceLastSpawn;
    protected internal Stack<GameObject> contents = new Stack<GameObject>();

    public GameObject[] virusPrefabs = new GameObject[1];
    public Properties properties;

    [System.Serializable]
    public class Properties {
        public bool randomizeTime = true;
        public int health = 3;
        public int spawnCounter = 0;
        public float spawnDelay = 5f;
        public float spawnDelayMinimum = 4f;
        public float spawnDelayMaximum = 6f;
        public float spawnRange = 2f;
        public float minimumDistance = 1f;
        public float newEnemySpeed = 1f;
    }

    protected Vector3 GetSpawnPosition() {
        float angle = Random.value * Mathf.PI;
        float radius = Mathf.Sqrt(Random.value) * (properties.spawnRange - properties.minimumDistance);
        float cos = Mathf.Cos(angle);
        float sin = Mathf.Sin(angle);
        float x = (radius + properties.minimumDistance) * cos;
        return new Vector3(x, 0, 0);
    }

    public bool CanSpawn() {
        return contents.Count > 0;
    }

    public void Spawn() {
        if (CanSpawn()) {
            GameObject obj = contents.Pop();
            obj.transform.position = transform.position + GetSpawnPosition();
            Util.SetCollisionsActive(obj, true);
            Util.SetRendererActive(obj, true);
            EnemyMovement self = gameObject.GetComponent<EnemyMovement>();
            EnemyMovement[] other = obj.GetComponents<EnemyMovement>();
            EnemyMovement[] otherChildren = obj.GetComponentsInChildren<EnemyMovement>();
            EnemyMovement[] others = new EnemyMovement[other.Length + otherChildren.Length];
            other.CopyTo(others, 0);
            otherChildren.CopyTo(others, other.Length);
            foreach (EnemyMovement movement in others) {
                movement.platformTrigger = self.platformTrigger;
                movement.speed = properties.newEnemySpeed * (Random.Range(0, 2) == 1 ? -1 : 1);
            }
        }
        timeSinceLastSpawn = 0;
    }

    public IEnumerator Kill() {
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
    }

    #region Unity methods
    public void Awake() {
        base.Awake();
        for (int i = 0; i < properties.spawnCounter; i++) {
            GameObject obj = Instantiate(virusPrefabs[Random.Range(0, virusPrefabs.Length)]) as GameObject;
            Util.SetCollisionsActive(obj, false);
            Util.SetRendererActive(obj, false);
            contents.Push(obj);
        }
    }

    public void Start() {
        health = properties.health;
    }

    override public void Update() {
        if (!GameController.Moveable || !IsVisible()) {
            return;
        }

        if (CanSpawn() && health > 0) {
            timeSinceLastSpawn += Time.deltaTime;
            float bounds = (properties.randomizeTime ? Random.Range(properties.spawnDelayMinimum, properties.spawnDelayMaximum) : properties.spawnDelay);
            if (timeSinceLastSpawn >= bounds) {
                Spawn();
            }
        } else {
            StartCoroutine("Kill");
        }
    }
    #endregion
}
