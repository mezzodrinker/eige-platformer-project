﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class EnemyMovement : MonoBehaviour
{

    [System.Serializable]
    public class CollisionProperties
    {
        public float strength;
        public Vector2 basePush;
    }

    public float speed;
    public float range;
    public PlatformTrigger platformTrigger;
    public CollisionProperties collisionProperties;

    private bool waiting;
    private bool patrolling;
    private int movingHash;
    private float timeToWait;
    private float timeWaited;
    private float timeToPatrol;
    private float timePatrolled;
    private Animator defenderAnim;
    private Rigidbody rigidbodySelf;
    private Rigidbody rigidbodyTarget;

    public bool Waiting
    {
        get
        {
            return waiting;
        }
    }

    public bool Patrolling
    {
        get
        {
            return patrolling;
        }
    }

    private void UpdateRigidbody()
    {
        rigidbodyTarget = GameController.Target.GetComponent<Rigidbody>();
        if (rigidbodyTarget == null)
        {
            Debug.LogError("Target does not have a rigidbody");
        }
    }

    private void ChangeDirection()
    {
        speed = -speed; //TODO: Lerp
    }

    #region Unity Methods
    void Awake()
    {
        movingHash = Animator.StringToHash("Moving");
        defenderAnim = gameObject.GetComponentInChildren<Animator>();
        rigidbodySelf = gameObject.GetComponent<Rigidbody>();
        patrolling = true;
        waiting = false;
        timePatrolled = 0.0f;
        timeToPatrol = 0.0f;
        timePatrolled = 0.0f;
        timeWaited = 0.0f;
    }

    void Start()
    {
        UpdateRigidbody();
    }

    void Update()
    {
        if (!GameController.Moveable)
        {
            return;
        }

        SetAnim();

        if (platformTrigger != null && platformTrigger.Triggered)
        {
            Vector3 enemyMovement = new Vector3((GameController.Target.transform.position - transform.position).normalized.x, 0.0f, 0.0f);
            enemyMovement = enemyMovement * Mathf.Abs(speed) * Time.deltaTime;
            rigidbodySelf.MovePosition(transform.position + enemyMovement);

        }
        else if (platformTrigger != null)
        {
            WaitFunction();

            PatrolFunction();
        }
    }

    void LateUpdate()
    {
        if (GameController.Target.rigidbody != rigidbodyTarget)
        {
            UpdateRigidbody();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == TagController.trigger)
        {
            ChangeDirection();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == platformTrigger.gameObject)
        {
            ChangeDirection();
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == GameController.Target.tag)
        {
            float velocity = rigidbodySelf.velocity.magnitude;
            float massRelation = rigidbodySelf.mass / rigidbodyTarget.mass;
            Vector2 hitPower = new Vector2(velocity * collisionProperties.strength * massRelation + Mathf.Sign(speed) * collisionProperties.basePush.x,
                velocity * collisionProperties.strength * massRelation + Mathf.Sign(speed) * collisionProperties.basePush.y);
            rigidbodyTarget.AddForce(hitPower);
        }
    }
    #endregion

    private void SetAnim()
    {
        if (Patrolling)
        {
            defenderAnim.SetBool(movingHash, true);
        }
        else
        {
            defenderAnim.SetBool(movingHash, false);
        }
    }

    private void WaitFunction()
    {
        if (waiting)
        {
            timeWaited += Time.deltaTime;
            if (timeToWait == 0.0f)
            {
                timeToWait = Random.Range(1.0f, 4.0f);
            }
            if (timeWaited >= timeToWait)
            {
                waiting = false;
                patrolling = true;
                timeWaited = 0.0f;
                timePatrolled = 0.0f;
            }
            else
            {
                rigidbodySelf.velocity = new Vector3();
            }
        }
    }

    private void PatrolFunction()
    {
        if (patrolling)
        {
            timePatrolled += Time.deltaTime;
            if (timeToPatrol == 0.0f)
            {
                timeToPatrol = Random.Range(2.0f, 6.0f);
            }
            if (timePatrolled >= timeToPatrol)
            {
                waiting = true;
                patrolling = false;
                timeToWait = 0.0f;
                timeToPatrol = 0.0f;
                timeWaited = 0.0f;
                timePatrolled = 0.0f;
            }
            else
            {
                rigidbodySelf.MovePosition(transform.position + speed * Time.deltaTime * Vector3.right);
            }
        }
    }

}
