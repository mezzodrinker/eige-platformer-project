﻿using UnityEngine;
using System.Collections;

public class VirusInfector : Enemy {

    public GameObject torus1;
    public GameObject torus2;
    public GameObject icoSphere;
    public Speeds speeds;
    [System.Serializable]
    public class Speeds
    {
        public float speedTorus1;
        public float speedTorus2;
        public float speedIco;
        public float accSpeedTorus1;
        public float accSpeedTorus2;
        public float accSpeedIco;
    }
    public float smoothTime;
    public Vector3 torus1Axis;
    public Vector3 torus2Axis;
    public Vector3 icoAxis;

    private float random;
    private float highValue;
    private float counter = 0;
    private float actualSpeedTorus1;
    private float actualSpeedTorus2;
    private float actualSpeedIco;
    private float smoothTolerance;

    void Rotate(GameObject gameObject, Vector3 axis, float speed)
    {
        gameObject.transform.Rotate(axis.normalized * speed * Time.deltaTime);
    }

    #region Unity Methods
    //called at the first frame the script is active
    void Start()
    {
        random = Random.Range(1f, 2f);
        highValue = Random.Range(40, 90);
        smoothTolerance = 0.1f;
        actualSpeedTorus1 = speeds.speedTorus1;
        actualSpeedTorus2 = speeds.speedTorus2;
        actualSpeedIco = speeds.speedIco;
    }

    //called every frame
    override public void Update()
    {
        if (counter >= highValue)
        {
            random = Random.Range(1f, 2f);
            highValue = Random.Range(50, 120);
            counter = 0;
        }
        if (random <= 1.5f)
        {
            actualSpeedTorus1 = Mathf.Lerp(actualSpeedTorus1, speeds.speedTorus1, smoothTime * Time.deltaTime);
            Rotate(torus1, torus1Axis, actualSpeedTorus1);
            actualSpeedTorus2 = Mathf.Lerp(actualSpeedTorus2, speeds.speedTorus2, smoothTime * Time.deltaTime);
            Rotate(torus2, torus2Axis, actualSpeedTorus2);
            actualSpeedIco = Mathf.Lerp(actualSpeedIco, speeds.speedIco, smoothTime * Time.deltaTime);
            Rotate(icoSphere, icoAxis, actualSpeedIco);

            counter += Time.deltaTime * 10;
        }
        else if (random > 1.5f)
        {
            actualSpeedTorus1 = Mathf.Lerp(actualSpeedTorus1, speeds.accSpeedTorus1, smoothTime * Time.deltaTime);
            Rotate(torus1, torus1Axis, actualSpeedTorus1);
            actualSpeedTorus2 = Mathf.Lerp(actualSpeedTorus2, speeds.accSpeedTorus2, smoothTime * Time.deltaTime);
            Rotate(torus2, torus2Axis, actualSpeedTorus2);
            actualSpeedIco = Mathf.Lerp(actualSpeedIco, speeds.accSpeedIco, smoothTime * Time.deltaTime);
            Rotate(icoSphere, icoAxis, actualSpeedIco);

            counter += Time.deltaTime * 50;
        }
    }
    #endregion

}
