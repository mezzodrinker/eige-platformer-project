﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class SpawnBehavior : MonoBehaviour {
    private bool savedUseGravity;
    private bool savedIsKinematic;
    private bool hasSpawned;
    private float timeWaited = 0;
    private Camera camera;

    public float spawnDelay = 1f;
    public GameObject spawnObject;
    public ParticleSystem particleSystem;

    public bool IsVisible() {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        return GeometryUtility.TestPlanesAABB(planes, gameObject.collider.bounds);
    }

    public void Spawn() {
        spawnObject.renderer.enabled = true;
        spawnObject.transform.position = transform.position;
        spawnObject.transform.rotation = transform.rotation;
        if (particleSystem != null) {
            particleSystem.Play();
        }
        if (spawnObject.collider != null) {
            spawnObject.collider.enabled = true;
        }
        if (spawnObject.rigidbody != null) {
            spawnObject.rigidbody.useGravity = savedUseGravity;
            spawnObject.rigidbody.isKinematic = savedIsKinematic;
        }
        hasSpawned = true;
    }

    #region Unity method
    public void Awake() {
        GameObject cameraGameObject = GameObject.FindGameObjectWithTag(TagController.camera);
        if (cameraGameObject == null) {
            Debug.LogError("No GameObject tagged with " + TagController.camera + " found!");
            return;
        }
        camera = cameraGameObject.camera;
        if (camera == null) {
            Debug.LogError("GameObject tagged with " + TagController.camera + " is not a camera!");
            return;
        }

        spawnObject.renderer.enabled = false;
        if (particleSystem != null) {
            particleSystem.Stop();
        }
        if (spawnObject.collider != null) {
            spawnObject.collider.enabled = false;
        }
        if (spawnObject.rigidbody != null) {
            savedUseGravity = spawnObject.rigidbody.useGravity;
            savedIsKinematic = spawnObject.rigidbody.isKinematic;
            spawnObject.rigidbody.useGravity = false;
            spawnObject.rigidbody.isKinematic = false;
        }
        hasSpawned = false;
    }

    public void FixedUpdate() {
        if (!GameController.Moveable)
            return;

        if (hasSpawned) {
            Destroy(gameObject);
            return;
        }

        if (IsVisible()) {
            Debug.Log("visible");
            timeWaited += Time.deltaTime;
            Debug.Log("time waited: " + timeWaited);

            if (timeWaited >= spawnDelay && !hasSpawned) {
                Spawn();
            }
        } else {
            timeWaited = 0;
        }
    }
    #endregion
}
