﻿using UnityEngine;
using System.Collections;

public abstract class Enemy : MonoBehaviour {
    private Camera camera;

    public bool IsVisible() {
        if (!Util.IsRendererActive(gameObject))
            return false;
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        return GeometryUtility.TestPlanesAABB(planes, gameObject.collider.bounds);
    }

    #region Unity methods
    public void Awake() {
        GameObject cameraGameObject = GameObject.FindGameObjectWithTag(TagController.camera);
        if (cameraGameObject == null) {
            Debug.LogError("No GameObject tagged with " + TagController.camera + " found!");
            return;
        }
        camera = cameraGameObject.camera;
        if (camera == null) {
            Debug.LogError("GameObject tagged with " + TagController.camera + " is not a camera!");
            return;
        }
    }

    abstract public void Update();
    #endregion
}
