﻿using UnityEngine;
using System.Collections;

public class ParticleMovingSystem : MonoBehaviour {

    private GameObject defender;
    private Transform parentPos;

	// Use this for initialization
	void Awake () {
        parentPos = GetComponentInParent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(parentPos.transform.position.x, parentPos.transform.transform.position.y, parentPos.transform.transform.position.z);
	}
}
