﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrojanInfector : Trojan {
    private int health;
    private float timeSinceLastSpawn = 0;
    private EnemyMovement movement;
    private Stack<VirusInfector> contents = new Stack<VirusInfector>();

    public TrojanInfectorProperties properties;
    public TrojanInfectorSpawnProperties spawnProperties;

    [System.Serializable]
    public class TrojanInfectorProperties {
        public int health = 3;
        public float movementRadius = 3;
        public VirusInfector[] contents = new VirusInfector[0];
    }

    [System.Serializable]
    public class TrojanInfectorSpawnProperties {
        public float timeToSpawn = 5f;
        public bool randomize = true;
        public float randomBounds = 0.75f;
        public Vector3 relativeSpawnLocation = new Vector3(-1, 0, 0);
    }

    public void Spawn() {
        if (contents.Count > 0) {
            VirusInfector v = contents.Pop();
            v.transform.position = transform.position + spawnProperties.relativeSpawnLocation;
            v.gameObject.renderer.enabled = true;
            v.gameObject.rigidbody.detectCollisions = true;
            v.gameObject.rigidbody.useGravity = true;
        }
        timeSinceLastSpawn = 0;
    }

    #region Unity methods
    public void Awake() {
        base.Awake();
        movement = gameObject.GetComponent<EnemyMovement>();
    }

    public void Start() {
        health = properties.health;
        foreach (VirusInfector v in properties.contents) {
            v.gameObject.renderer.enabled = false;
            v.gameObject.rigidbody.detectCollisions = false;
            v.gameObject.rigidbody.useGravity = false;
            contents.Push(v);
        }
    }

    override public void Update() {
        if (!GameController.Moveable)
            return;

        Debug.Log(IsVisible());

        timeSinceLastSpawn += Time.deltaTime;

        float bounds = spawnProperties.timeToSpawn;
        if (spawnProperties.randomize) {
            bounds += Random.Range(-spawnProperties.randomBounds, spawnProperties.randomBounds);
        }
        if (timeSinceLastSpawn >= bounds) {
            Spawn();
        }
    }
    #endregion
}
