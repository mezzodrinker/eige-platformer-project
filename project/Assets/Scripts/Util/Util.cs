﻿using UnityEngine;
using System.Collections;

public class Util {
    private static Camera mainCamera = null;

    public static Camera MainCamera {
        get {
            if (mainCamera == null) {
                GameObject cameraGameObject = GameObject.FindGameObjectWithTag(TagController.camera);
                if (cameraGameObject == null) {
                    Debug.LogError("No GameObject tagged with " + TagController.camera + " found!");
                    return null;
                }
                mainCamera = cameraGameObject.camera;
                if (mainCamera == null) {
                    Debug.LogError("GameObject tagged with " + TagController.camera + " is not a camera!");
                    return null;
                }
            }
            return mainCamera;
        }
    }

    public static bool IsVisible(GameObject gameObject, Bounds bounds) {
        if (!IsRendererActive(gameObject))
            return false;
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(MainCamera);
        return GeometryUtility.TestPlanesAABB(planes, gameObject.collider.bounds);
    }

    public static void SetRendererActive(GameObject gameObject, bool active) {
        foreach (Renderer renderer in gameObject.GetComponents<Renderer>()) {
            renderer.enabled = active;
        }
        foreach (Renderer renderer in gameObject.GetComponentsInChildren<Renderer>()) {
            renderer.enabled = active;
        }
    }

    public static bool IsRendererActive(GameObject gameObject) {
        foreach (Renderer renderer in gameObject.GetComponents<Renderer>()) {
            if (renderer.enabled)
                return true;
        }
        foreach (Renderer renderer in gameObject.GetComponentsInChildren<Renderer>()) {
            if (renderer.enabled)
                return true;
        }
        return false;
    }

    public static bool IsColliderEnabled(GameObject gameObject) {
        foreach (Rigidbody rigidbody in gameObject.GetComponents<Rigidbody>()) {
            if (rigidbody.detectCollisions)
                return true;
        }
        foreach (Rigidbody rigidbody in gameObject.GetComponentsInChildren<Rigidbody>()) {
            if (rigidbody.detectCollisions)
                return true;
        }
        return false;
    }

    public static void SetCollisionsActive(GameObject gameObject, bool active) {
        foreach (Rigidbody rigidbody in gameObject.GetComponents<Rigidbody>()) {
            rigidbody.detectCollisions = active;
        }
        foreach (Rigidbody rigidbody in gameObject.GetComponentsInChildren<Rigidbody>()) {
            rigidbody.detectCollisions = active;
        }
    }
}
