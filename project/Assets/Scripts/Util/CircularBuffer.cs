public class CircularBuffer<T> {
    private T[] buffer;
    private int startPos;
    private int endPos;
    private readonly int size;

    public CircularBuffer(int size) {
        this.size = size;
        buffer = new T[size];
        startPos = 0;
        endPos = 0;
    }

    public int Count {
        get {
            return (endPos - startPos + size) % size;
        }
    }

    public void Push(T o) {
        buffer[endPos] = o;
        endPos = ++endPos % size;
        if (endPos == startPos) {
            startPos = ++startPos % size;
        }
    }

    public T Pop() {
        return endPos == startPos ? default(T) : buffer[(endPos = (--endPos + size) % size)];
    }

    public void Clear() {
        for (int i = 0; i < buffer.Length; i++) {
            buffer[i] = default(T);
        }
        startPos = 0;
        endPos = 0;
    }
}