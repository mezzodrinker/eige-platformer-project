﻿public abstract class TimeReverseObject {}

public interface ITimeReverseBehavior {
    TimeReverseObject SaveTimeReverseObject();

    void LoadTimeReverseObject(TimeReverseObject o);
}