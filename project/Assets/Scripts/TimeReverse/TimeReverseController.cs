﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ITimeReverseBehavior))]
public class TimeReverseController : MonoBehaviour {
    private CircularBuffer<TimeReverseObject> buffer    = new CircularBuffer<TimeReverseObject>(1500);
    private ITimeReverseBehavior script                 = null;

    public void Clear() {
        buffer.Clear();
    }

    public bool HasFinished() {
        return buffer.Count <= 0;
    }

    #region Unity methods
    public void Start() {
        // receive the ITimeReverseBehavior component of the current game object
        script = gameObject.GetComponent(typeof(ITimeReverseBehavior)) as ITimeReverseBehavior;
    }

    public void FixedUpdate() {
        if (GameController.ReverseTime && !HasFinished()) {
            script.LoadTimeReverseObject(buffer.Pop());
        } else {
            buffer.Push(script.SaveTimeReverseObject());
        }
    }
    #endregion
}
