﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody), typeof(Collider))]
public class PlatformFalling : MonoBehaviour, ITimeReverseBehavior, IPlatform {
    public MovementSettings movementSettings;

    [System.Serializable]
    public class MovementSettings {
        // internal attributes
        internal bool appliedForce = false;
        internal Rigidbody rigidbody;
        internal bool isFalling = false;
        internal bool fell = false;

        // general settings
        public bool enableStopFalling = false;
        public bool playerTriggerOnly = false;
        public float triggerDelay = 0f;
        public float fallForce = 1f;
        public Vector3 startPosition;
        public Vector3 startRotation;
        public Vector3 stopPosition;

        public void AddFallForce() {
            if (appliedForce)
                return;

            rigidbody.AddForce(fallForce * Vector3.down);
            rigidbody.useGravity = true;
            rigidbody.isKinematic = false;
            isFalling = true;
            appliedForce = true;
        }
    }

    public TimeReverseObject SaveTimeReverseObject() {
        PlatformTimeReverseObject ptro = new PlatformTimeReverseObject();
        ptro.position = transform.position;
        ptro.rotation = transform.rotation;
        return ptro;
    }

    public void LoadTimeReverseObject(TimeReverseObject o) {
        PlatformTimeReverseObject ptro = o as PlatformTimeReverseObject;
        if (ptro == null) {
            Debug.LogError("PlatformFalling.LoadTimeReverseObject: parameter is not a PlatformTimeReverseObject");
            return;
        }
        transform.position = ptro.position;
        transform.rotation = ptro.rotation;
    }

    public IEnumerator WaitAndFall() {
        yield return new WaitForSeconds(movementSettings.triggerDelay);
        movementSettings.AddFallForce();
    }

    #region Unity methods
    public void OnCollisionEnter(Collision collision) {
        // if the game controller decides that stuff can't be moved around, exit this method
        if (!GameController.Moveable)
            return;

        if (movementSettings.playerTriggerOnly) {
            if (collision.gameObject.tag != TagController.player) {
                return;
            }
        }
        StartCoroutine("WaitAndFall");
    }

    public void Start() {
        movementSettings.rigidbody = gameObject.GetComponent<Rigidbody>();
        movementSettings.rigidbody.useGravity = false;
        movementSettings.rigidbody.isKinematic = true;
        movementSettings.rigidbody.freezeRotation = true;
        movementSettings.startPosition = transform.position;
        movementSettings.startRotation = transform.rotation.eulerAngles;
    }

    public void Update() {
        // if the game controller decides that stuff can't be moved around, exit this method
        if (!GameController.Moveable)
            return;

        // if this platform isn't falling, there's nothing to do
        if (!movementSettings.isFalling)
            return;

        // if the platform is going to stop anywhere, make sure it actually does.
        if (Vector3.Distance(transform.position, movementSettings.stopPosition) < 0.1f * movementSettings.rigidbody.velocity.magnitude) {
            // stop the platform and set its position to the stop position
            movementSettings.rigidbody.velocity = new Vector3();
            movementSettings.rigidbody.isKinematic = true;
            movementSettings.rigidbody.useGravity = false;
            movementSettings.rigidbody.freezeRotation = false;
            movementSettings.rigidbody.transform.position = movementSettings.stopPosition;
            movementSettings.rigidbody.transform.rotation = Quaternion.Euler(movementSettings.startRotation);
            movementSettings.isFalling = false;
            movementSettings.fell = true;
        }
    }
    #endregion

    private class PlatformTimeReverseObject : TimeReverseObject {
        public Vector3 position;
        public Quaternion rotation;
    }
}