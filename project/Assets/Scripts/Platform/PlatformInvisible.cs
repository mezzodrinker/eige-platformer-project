﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlatformInvisible : MonoBehaviour, IPlatform {
    #region Unity methods
    public void Start() {
        Rigidbody rigidbody = gameObject.GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
        rigidbody.isKinematic = true;
        gameObject.renderer.enabled = false;
    }
    #endregion
}
