﻿using UnityEngine;
using System.Collections;

public class PlatformTrigger : MonoBehaviour {
    private bool triggered = false;
    private BoxCollider trigger;

    public bool Triggered {
        get {
            return triggered;
        }
    }

    public BoxCollider Trigger {
        get {
            return trigger;
        }
    }

    #region Unity methods
    public void Awake() {
        trigger = gameObject.AddComponent<BoxCollider>();
        trigger.size = new Vector3(10.0f, 50.0f, 1.0f);
        trigger.center = new Vector3(5.0f, 20.0f, 0.0f);
        trigger.isTrigger = true;
    }

    public void OnTriggerEnter(Collider collider) {
        if (collider.gameObject == GameController.Target) {
            triggered = true;
        }
    }

    public void OnTriggerExit(Collider collider) {
        if (collider.gameObject == GameController.Target) {
            triggered = false;
        }
    }
    #endregion
}
