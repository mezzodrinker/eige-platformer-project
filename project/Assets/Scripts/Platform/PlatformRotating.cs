﻿using UnityEngine;
using System.Collections;

public class PlatformRotating : MonoBehaviour, ITimeReverseBehavior, IPlatform {
    public MovementSettings movementSettings;

    [System.Serializable]
    public class MovementSettings {
        // general settings
        public float rotationSpeed = 1f;
        public Vector3 rotationAxis;
        public Vector3 startPosition;
        public Vector3 startRotation;
    }

    public TimeReverseObject SaveTimeReverseObject() {
        PlatformTimeReverseObject ptro = new PlatformTimeReverseObject();
        ptro.position = transform.position;
        ptro.rotation = transform.rotation;
        return ptro;
    }

    public void LoadTimeReverseObject(TimeReverseObject o) {
        PlatformTimeReverseObject ptro = o as PlatformTimeReverseObject;
        if (ptro == null) {
            Debug.LogError("PlatformRotating.LoadTimeReverseObject: parameter is not a PlatformTimeReverseObject");
            return;
        }
        transform.position = ptro.position;
        transform.rotation = ptro.rotation;
    }

    #region Unity methods
    public void Start() {
        movementSettings.startPosition = transform.position;
        movementSettings.startRotation = transform.rotation.eulerAngles;
    }

    public void Update() {
        // if the game controller decides that stuff can't be moved around, exit this method
        if (!GameController.Moveable) return;

        // rotate the platform
        transform.Rotate(movementSettings.rotationAxis, movementSettings.rotationSpeed * Time.deltaTime);
    }
    #endregion

    private class PlatformTimeReverseObject : TimeReverseObject {
        public Vector3 position;
        public Quaternion rotation;
    }
}
