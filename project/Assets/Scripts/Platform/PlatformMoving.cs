﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class PlatformMoving : MonoBehaviour, ITimeReverseBehavior, IPlatform {
    private float timeWaited = 0.0f;
    private GameObject toMove = null;

    public MovementSettings movementSettings;

    [System.Serializable]
    public class MovementSettings {
        // private attributes
        private bool reversedMovement = false;
        private int currentTarget = 0;

        // general settings
        public bool cycleCheckpoints = true;
        public float movementSpeed = 1.0f;
        public float waitAtCheckpointFor = 1.0f;
        public Vector3 startPosition;
        public Vector3 startRotation;

        // checkpoint settings
        public Transform[] checkpoints = new Transform[2];

        public Transform CurrentTarget {
            get {
                return checkpoints[currentTarget];
            }
        }

        public bool ReversedMovement {
            get {
                return reversedMovement && !cycleCheckpoints;
            }
            set {
                reversedMovement = value && !cycleCheckpoints;
            }
        }

        public Transform NextTarget() {

            // determine next target index
            if (ReversedMovement) {
                // check for possibly invalid indexes
                if (currentTarget - 1 < 0) {
                    // toggle reversed movement and increase current target index by 1
                    ReversedMovement = false;
                    currentTarget++;
                } else {
                    // decrease the current target index by 1
                    currentTarget--;
                }
            } else {
                // check for possibly invalid indexes
                if (currentTarget + 1 >= checkpoints.Length) {
                    if (cycleCheckpoints) {
                        // get back to the first target
                        currentTarget = 0;
                    } else {
                        // toggle reversed movement and decrease current target index by 1
                        ReversedMovement = true;
                        currentTarget--;
                    }
                } else {
                    // increase the current target index by 1
                    currentTarget++;
                }
            }

            // finally, return the (new) current target
            return CurrentTarget;
        }
    }

    public TimeReverseObject SaveTimeReverseObject() {
        PlatformTimeReverseObject ptro = new PlatformTimeReverseObject();
        ptro.position = transform.position;
        ptro.rotation = transform.rotation;
        return ptro;
    }

    public void LoadTimeReverseObject(TimeReverseObject o) {
        PlatformTimeReverseObject ptro = o as PlatformTimeReverseObject;
        if (ptro == null) {
            Debug.LogError("PlatformMoving.LoadTimeReverseObject: parameter is not a PlatformTimeReverseObject");
            return;
        }
        transform.position = ptro.position;
        transform.rotation = ptro.rotation;
    }

    #region Unity methods
    public void Start() {
        movementSettings.startPosition = transform.position;
        movementSettings.startRotation = transform.rotation.eulerAngles;
    }

    public void Update() {
        // if the game controller decides that stuff can't be moved around, exit this method
        if(!GameController.Moveable) return;

        // check if current target checkpoint has been reached
        if(Vector3.Distance(transform.position, movementSettings.CurrentTarget.position) <= 0.01f * movementSettings.movementSpeed) {
            movementSettings.NextTarget();
            timeWaited = 0;
        }

        // if the platform still has to wait, increase the amount of waited time and return.
        if (timeWaited < movementSettings.waitAtCheckpointFor) {
            timeWaited += Time.deltaTime;
            return;
        }

        // calculate direction of target checkpoint relative to current position
        Vector3 direction = movementSettings.CurrentTarget.position - transform.position;
        Vector3 movement = direction.normalized * Time.deltaTime * movementSettings.movementSpeed;
        transform.position += movement;
        if (toMove != null) {
            toMove.transform.position += movement;
        }
    }

    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == TagController.player) {
            toMove = collision.gameObject;
        }
    }

    void OnCollisionExit(Collision collision) {
        if (collision.gameObject.tag == TagController.player) {
            toMove = null;
        }
    }
    #endregion

    private class PlatformTimeReverseObject : TimeReverseObject {
        public Vector3 position;
        public Quaternion rotation;
    }
}
