## Style Guide ##
###Regions###
**Unity Methods**

	Around all Unity Methods a region called "Unity Methods"

###Unity Method Comments###
**Awake**

	//called at the beginning of the game

**Start**

	//called at the first frame the script is active

**FixedUpdate**

	//called every 0.02 seconds (interval can be changed)

**Update**

	//called every frame

**LateUpdate**

	//called after the last update per frame

**OnTriggerEnter**

	//called whenever an other collider hits the objects trigger collider